package com.jason.library.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.jason.library.model.Authors;
import com.jason.library.model.Books;
import com.jason.library.model.Junction;

@Repository
@Transactional
public class LibraryRepository {
	@PersistenceContext
	EntityManager entityManager;
	
	public List<Books> getAllBooks(){
		TypedQuery<Books> query = entityManager.createQuery("SELECT b " + 
				"FROM Books b", Books.class);
		List<Books> books = query.getResultList();
		return books;
	}
	
	public List<Books> getBookById(int book_id){
		TypedQuery<Books> query = entityManager.createQuery("SELECT b " + 
				"FROM Books b " + 
				"WHERE b.book_id=:book_id", Books.class);
		query.setParameter("book_id", book_id);
		List<Books> books = query.getResultList();
		return books;
	}
	
	public List<Authors> getAllAuthors(){
		TypedQuery<Authors> query = entityManager.createQuery("SELECT a " + 
				"FROM Authors a", Authors.class);
		List<Authors> authors = query.getResultList();
		return authors;
	}
	
	public List<Authors> getAuthorById(int author_id){
		TypedQuery<Authors> query = entityManager.createQuery("SELECT a " + 
				"FROM Authors a " + 
				"WHERE a.author_id=:author_id", Authors.class);
		query.setParameter("author_id", author_id);
		List<Authors> authors = query.getResultList();
		return authors;
	}
	
	public List<Junction> getBookByAuthor(String author_id){
		TypedQuery<Junction> query = entityManager.createQuery("SELECT j " + 
				"FROM Junction j " + 
				"JOIN j.authors a " + 
				"WHERE a.name =: name", Junction.class);
		query.setParameter("name", author_id);
		List<Junction> junctions = query.getResultList();
		return junctions;
	}
	
	public void insertNewBook(Books books) {
		entityManager.createNativeQuery("INSERT INTO Books (title, isbn, cover_type) VALUES (?, ?, ?)")
			.setParameter(1, books.getTitle())
			.setParameter(2, books.getIsbn())
			.setParameter(3, books.getCover_type())
			.executeUpdate();
	}
	
	public void insertNewAuthor(Authors authors) {
		entityManager.createNativeQuery("INSERT INTO Authors (name, age, email) VALUE (?, ?, ?)")
			.setParameter(1, authors.getName())
			.setParameter(2, authors.getAge())
			.setParameter(3, authors.getEmail())
			.executeUpdate();	
	}
	
	public List<Books> getBookByName(String bookName){
		TypedQuery<Books> query = entityManager.createQuery("SELECT b " + 
				"FROM Books b " + 
				"WHERE b.title=:title", Books.class);
		query.setParameter("title", bookName);
		List<Books> books = query.getResultList();
		return books;
	}
	
	public List<Authors> getAuthorByName(String authorName) {
		TypedQuery<Authors> query = entityManager.createQuery("SELECT a " + 
				"FROM Authors a " + 
				"WHERE a.name=:name", Authors.class);
		query.setParameter("name", authorName);
		List<Authors> authors = query.getResultList();
		return authors;
	}
	
	public void insertNewJunction(int authorId, int bookId) {
		entityManager.createNativeQuery("INSERT INTO Junction (author_id, book_id) VALUES (?, ?)")
			.setParameter(1, authorId)
			.setParameter(2, bookId)
			.executeUpdate();
	}
}
