package com.jason.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jason.library.controller.LibraryController;
import com.jason.library.jpa.LibraryRepository;
import com.jason.library.view.LibraryView;

@SpringBootApplication
public class LibraryApplication implements CommandLineRunner{
	@Autowired
	LibraryRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(LibraryApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception{
		LibraryView view = new LibraryView();
		LibraryController controller = new LibraryController(view);
		
		while(true) {
			controller.mainMethod(repository);
		}
	}

}
