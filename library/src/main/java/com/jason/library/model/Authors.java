package com.jason.library.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "Authors")
@Table(name = "Authors")
public class Authors {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "author_id")
	int author_id;
	String name;
	int age;
	String email;
	
	@OneToMany(mappedBy = "authors")
	Set<Junction> junction;
	
	public Authors(int author_id, String name, int age, String email) {
		super();
		this.author_id = author_id;
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public Authors(String name, int age, String email) {
		super();
		this.name = name;
		this.age = age;
		this.email = email;
	}

	public Authors() {
		super();
	}

	public int getId_author() {
		return author_id;
	}

	public void setAuthor_id(int id_author) {
		this.author_id = id_author;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Authors [id_author=" + author_id + ", name=" + name + ", age=" + age + ", email=" + email + "]";
	}
}
