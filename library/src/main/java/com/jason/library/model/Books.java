package com.jason.library.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name = "Books")
@Table(name = "Books")
public class Books {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "book_id")
	int book_id;
	String title;
	String isbn;
	String cover_type;
	
	@OneToMany(mappedBy = "books")
	Set<Junction> junction;
	
	public Books(int book_id, String title, String isbn, String cover_type) {
		super();
		this.book_id = book_id;
		this.title = title;
		this.isbn = isbn;
		this.cover_type = cover_type;
	}

	public Books(String title, String isbn, String cover_type) {
		super();
		this.title = title;
		this.isbn = isbn;
		this.cover_type = cover_type;
	}

	public Books() {
		super();
	}

	public int getBook_id() {
		return book_id;
	}

	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getCover_type() {
		return cover_type;
	}

	public void setCover_type(String cover_type) {
		this.cover_type = cover_type;
	}

	@Override
	public String toString() {
		return "Books [book_id=" + book_id + ", title=" + title + ", isbn=" + isbn + ", cover_type=" + cover_type + "]";
	}
}
