package com.jason.library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name = "Junction")
@Table(name = "Junction")
public class Junction {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	int id;
	
	@ManyToOne
	@JoinColumn(name = "book_id")
	Books books;
	
	@ManyToOne
	@JoinColumn(name = "author_id")
	Authors authors;

	public Junction(int id, Books books, Authors authors) {
		super();
		this.id = id;
		this.books = books;
		this.authors = authors;
	}

	public Junction(Books books, Authors authors) {
		super();
		this.books = books;
		this.authors = authors;
	}

	public Junction() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Books getBooks() {
		return books;
	}

	public void setBooks(Books books) {
		this.books = books;
	}

	public Authors getAuthors() {
		return authors;
	}

	public void setAuthors(Authors authors) {
		this.authors = authors;
	}

	@Override
	public String toString() {
		return "Junction [id=" + id + ", books=" + books + ", authors=" + authors + "]";
	}
}
