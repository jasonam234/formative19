package com.jason.library.controller;

import java.util.List;
import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;

import com.jason.library.jpa.LibraryRepository;
import com.jason.library.model.Authors;
import com.jason.library.model.Books;
import com.jason.library.model.Junction;
import com.jason.library.view.LibraryView;

public class LibraryController {
	@Autowired
	LibraryRepository repository;
	
	Scanner scanner = new Scanner(System.in);
	
	private LibraryView view;
	
	public LibraryController() {}
	
	public LibraryController(LibraryView view) {
		this.view = view;
	}
	
	public void mainMethod(LibraryRepository repository) {
		int choice;
		
		view.header();
		choice = scanner.nextInt();
		scanner.nextLine();
		
		if(choice == 1) {
			getAllBooks(repository);
		}else if(choice == 2) {
			getAllAuthors(repository);
		}else if(choice == 3) {
			getBookById(repository);
		}else if(choice == 4) {
			getAuthorById(repository);
		}else if(choice == 5) {
			getBookByAuthor(repository);
		}else if(choice == 6) {
			addNewBook(repository);
		}else if(choice == 7) {
			addNewAuthor(repository);
		}else if(choice == 8) {
			addNewJunction(repository);
		}else if(choice == 0) {
			System.exit(0);
		}else {
			System.out.println("Invalid Input");
		}
	}
	
	public void getAllBooks(LibraryRepository repository) {
		List<Books> books = repository.getAllBooks();
		view.allBooks(books);
		scanner.nextLine();
	}
	
	public void getBookById(LibraryRepository repository) {
		int bookId;
		
		System.out.print("Book ID : ");
		bookId = scanner.nextInt();
		scanner.nextLine();
		try {
			List<Books> books = repository.getBookById(bookId);
			view.bookId(books);
			scanner.nextLine();
		}catch(Exception e) {
			view.bookIdError();
			scanner.nextLine();
		}
	}
	
	public void getAllAuthors(LibraryRepository repository) {
		List<Authors> authors = repository.getAllAuthors();
		view.allAuthors(authors);
		scanner.nextLine();
	}
	
	public void getAuthorById(LibraryRepository repository) {
		int authorId;
		
		System.out.print("Author ID : ");
		authorId = scanner.nextInt();
		scanner.nextLine();
		try {
			List<Authors> authors = repository.getAuthorById(authorId);
			view.authorId(authors);
			scanner.nextLine();
		}catch(Exception e) {
			view.authorIdError();
			scanner.nextLine();
		}
	}
	
	public void getBookByAuthor(LibraryRepository repository) {
		String authorName;
		
		System.out.print("Author Name : ");
		authorName = scanner.nextLine();
		List<Junction> junction = repository.getBookByAuthor(authorName);
		if(junction.size() == 0) {
			view.bookByAuthorError();
			scanner.nextLine();
		}else {
			view.bookByAuthorName(junction);
			scanner.nextLine();
		}
	}

	public void addNewBook(LibraryRepository repository) {
		String title;
		String isbn;
		String coverType;
		
		System.out.print("Book Title : ");
		title = scanner.nextLine();
		System.out.print("Book ISBN  : ");
		isbn = scanner.nextLine();
		System.out.print("Book Cover : ");
		coverType = scanner.nextLine();
		
		repository.insertNewBook(new Books(title, isbn, coverType));
	}

	public void addNewAuthor(LibraryRepository repository) {
		String name;
		int age;
		String email;
		
		System.out.print("Author Name  : ");
		name = scanner.nextLine();
		System.out.print("Author Age   : ");
		age = scanner.nextInt();
		scanner.nextLine();
		System.out.print("Author Email : ");
		email = scanner.nextLine();
		
		repository.insertNewAuthor(new Authors(name, age, email));
	}

	public void addNewJunction(LibraryRepository repository) {
		String bookTitle;
		String authorName;
		int authorId;
		int bookId;
		
		System.out.print("Author Name : ");
		authorName = scanner.nextLine();
		authorId = repository.getAuthorByName(authorName).get(0).getId_author();
		System.out.print("Book Title  : ");
		bookTitle = scanner.nextLine();
		bookId = repository.getBookByName(bookTitle).get(0).getBook_id();
		
		repository.insertNewJunction(authorId, bookId);
	}
}
