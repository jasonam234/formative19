package com.jason.library.view;

import java.util.List;

import com.jason.library.model.Authors;
import com.jason.library.model.Books;
import com.jason.library.model.Junction;

public class LibraryView {
	public void header() {
		System.out.println("==================================");
		System.out.println("Library Admin Page");
		System.out.println("==================================");
		System.out.println("1. Query all books");
		System.out.println("2. Query all authors");
		System.out.println("3. Query book by id");
		System.out.println("4. Query author by id");
		System.out.println("5. Query book by author");
		System.out.println("6. Insert new book");
		System.out.println("7. Insert new author");
		System.out.println("8. Insert new Junction");
		System.out.println("0. Exit");
		System.out.print("Input : ");
	}
	
	public void allBooks(List<Books> books) {
		System.out.println("==================================");
		for(int i = 0; i < books.size(); i++) {
			System.out.println("ID         : " + books.get(i).getBook_id());
			System.out.println("Title      : " + books.get(i).getTitle());
			System.out.println("Cover Type : " + books.get(i).getCover_type());
			System.out.println("ISBN       : " + books.get(i).getIsbn());
			System.out.println("==================================");
		}
		System.out.println("Press any key to continue . . .");
	}
	
	public void bookId(List<Books> books) {
		System.out.println("==================================");
		System.out.println("ID         : " + books.get(0).getBook_id());
		System.out.println("Title      : " + books.get(0).getTitle());
		System.out.println("Cover Type : " + books.get(0).getCover_type());
		System.out.println("ISBN       : " + books.get(0).getIsbn());
		System.out.println("==================================");
		System.out.println("Press any key to continue . . .");
	}
	
	public void allAuthors(List<Authors> authors) {
		System.out.println("==================================");
		for(int i = 0; i < authors.size(); i++) {
			System.out.println("ID         : " + authors.get(i).getId_author());
			System.out.println("Name       : " + authors.get(i).getName());
			System.out.println("Age        : " + authors.get(i).getAge());
			System.out.println("Email      : " + authors.get(i).getEmail());
			System.out.println("==================================");
		}
		System.out.println("Press any key to continue . . .");
	}
	
	public void authorId(List<Authors> authors) {
		System.out.println("==================================");
		System.out.println("ID         : " + authors.get(0).getId_author());
		System.out.println("Name       : " + authors.get(0).getName());
		System.out.println("Age        : " + authors.get(0).getAge());
		System.out.println("Email      : " + authors.get(0).getEmail());
		System.out.println("==================================");
		System.out.println("Press any key to continue . . .");
	}
	
	public void bookByAuthorName(List<Junction> junction) {
		System.out.println("==================================");
		for(int i = 0; i < junction.size(); i++) {
			System.out.println("Book ID      : " + junction.get(i).getBooks().getBook_id());
			System.out.println("Book Title   : " + junction.get(i).getBooks().getTitle());
			System.out.println("Author Name  : " + junction.get(i).getAuthors().getName());
			System.out.println("Author Email : " + junction.get(i).getAuthors().getEmail());
			System.out.println("==================================");
		}
		System.out.println("Press any key to continue . . .");
	}
	
	public void bookIdError() {
		System.out.println("Book ID not found");
		System.out.println("Press any key to continue . . .");
		System.out.println("==================================");
	}
	
	public void authorIdError() {
		System.out.println("Author ID not found");
		System.out.println("Press any key to continue . . .");
		System.out.println("==================================");
	}
	
	public void bookByAuthorError() {
		System.out.println("==================================");
		System.out.println("Book not found");
		System.out.println("Press any key to continue . . .");
		System.out.println("==================================");
	}
}
