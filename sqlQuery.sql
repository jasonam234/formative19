show databases;
create database library;
use library;

CREATE TABLE Books(
	book_id int UNIQUE NOT NULL AUTO_INCREMENT,
    title varchar(30),
    isbn varchar(30),
    cover_type varchar(30),
    
    PRIMARY KEY(book_id)
);

CREATE TABLE Authors(
	author_id int UNIQUE NOT NULL AUTO_INCREMENT,
    name varchar(30),
    age int,
    email varchar(30),
    
    PRIMARY KEY(author_id)
);

CREATE TABLE Junction(
	id int UNIQUE NOT NULL AUTO_INCREMENT,
    author_id int,
    book_id int,
    
    PRIMARY KEY(id),
    FOREIGN KEY(author_id) REFERENCES authors(author_id),
    FOREIGN KEY(book_id) REFERENCES books(book_id)
);

INSERT INTO Books(title, isbn, cover_type) VALUES
("Book One", "0010", "Paper Back"),
("Book Two", "0020", "Paper Back"),
("Book Three", "0030", "Hard Cover"),
("Book Four", "0040", "Paper Back"),
("Book Five", "0050", "Hard Cover"),
("Book Six", "0060", "Paper Back");

SELECT book_id, title, isbn, cover_type FROM Books;
SELECT book_id, title, isbn, cover_type FROM Books WHERE book_id = 1;


INSERT INTO Authors(name, age, email) VALUES
("Author A", 60, "AuthorA@email.com"),
("Author B", 22, "AuthorB@email.com"),
("Author C", 35, "AuthorC@email.com");

SELECT author_id, name, age, email FROM Authors;
SELECT author_id, name, age, email FROM Authors WHERE author_id = 1;

INSERT INTO junction(author_id, book_id) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 4),
(3, 5),
(3, 6);

SELECT b.title, a.name, a.age, a.email, b.isbn, b.cover_type
FROM Junction j
JOIN Books b ON b.book_id = j.book_id
JOIN Authors a ON a.author_id = j.author_id
WHERE a.name = "Author A";